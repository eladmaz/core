module org.ssldev.core {
	exports org.ssldev.core.mgmt;
	exports org.ssldev.core.services;
	exports org.ssldev.core.utils;
	exports org.ssldev.core.messages;
}