package org.ssldev.core.mgmt;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import org.ssldev.core.messages.LogMessage;
import org.ssldev.core.messages.MessageIF;
import org.ssldev.core.utils.Logger;
import org.ssldev.core.utils.SysInfo;
/**
 * a blocking queue designed to allow asynchronous "put" and "get" calls 
 */
public final class  AsyncEventHub extends EventHub {
	private ExecutorService exec ;
	
	public AsyncEventHub(int numthreads) {
		int availThreads = Runtime.getRuntime().availableProcessors() + 1;
		Logger.debug(this, "creating executor service with " + numthreads + " / " + availThreads + "(available) threads");
		
		exec = Executors.newFixedThreadPool(numthreads);
	}

	final Consumer<MessageIF> msgConsumer = m -> super.add(m);
	
	@Override
	public void add(MessageIF msg) {
		
		try {
			exec.execute(new Runnable() {
				@Override
				public void run() {
					
					long start = System.currentTimeMillis();
					
					Thread.currentThread().setName("THREAD"+Thread.currentThread().getId()+" - "+msg.getName());
					String tname = Thread.currentThread().getName();
					if(!(msg instanceof LogMessage) && Logger.isTraceEnabled()) {
						Logger.trace(this, "["+tname+"] working on event: [" + msg + "]");
					}

					msgConsumer.accept(msg);
					
					long duration = System.currentTimeMillis()-start;
					if(duration > 5 * 1000) { // more than 5 seconds
						Logger.warn(this, Thread.currentThread().getName() + " execution slowness detected ( execution took "+
								SysInfo.getTimeInMinutesSeconds(duration)+")");
					}
				}
			});
		} catch (RejectedExecutionException e) {
			Logger.warn(this, msg +" was rejected");
		}
	}
	
	@Override
	public void invokeLater(Runnable task) {
		try {
			exec.execute(task);	
		} catch (RejectedExecutionException e) {
			Logger.warn(this, "task exec request was rejected:\n"+e);
		}
	}
	
	@Override
	public void shutdown() {
		super.shutdown();
		
		exec.shutdown();
		try {
			if(!exec.awaitTermination(50, TimeUnit.MILLISECONDS)) {
				List<Runnable> forced = exec.shutdownNow();
				forced.forEach(c-> Logger.warn(this, "forced shutdown to " +c));
			}
		} catch (InterruptedException e) {
			exec.shutdownNow();
			// Preserve interrupt status
	        Thread.currentThread().interrupt();
		}
	}
}
