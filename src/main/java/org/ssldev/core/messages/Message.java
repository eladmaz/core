package org.ssldev.core.messages;

import java.util.UUID;

public class Message implements MessageIF {
	
	private UUID id = new UUID(0L, 0L);
	
	@Override
	public String toString() {
		return getName() + ": ";
	}

	@Override
	public void setTransactionId(UUID id) {
		this.id = id;
	}

	@Override
	public UUID getTransactionId() {
		return id;
	}
}
