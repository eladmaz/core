package org.ssldev.core.messages;

import java.util.UUID;

public interface MessageIF {

	public default String getName() {
		return this.getClass().getSimpleName();
	}
	
	/**
	 * set the transaction ID that the message should be associated with. the ID is a way to 
	 * trace a transaction across numerous service boundaries, or map processing to a specific 
	 * entity (e.g. user ID)
	 * 
	 * @param id to set
	 */
	void setTransactionId(UUID id);
	
	/**
	 * get the transaction ID that this message has been associated with. ID may be empty (i.e. 0L) if 
	 * message was not associated with a transaction
	 * 
	 * @return ID associated with message 
	 */
	UUID getTransactionId();
}
