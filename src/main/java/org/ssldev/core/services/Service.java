package org.ssldev.core.services;

import org.ssldev.core.messages.Message;
import org.ssldev.core.messages.MessageIF;
import org.ssldev.core.mgmt.EventHub;
import org.ssldev.core.utils.Logger;
/**
 * abstract service class
 * 
 * Each service should provide which messages it consumes and produces, for example: <br>
 * Consumes: 
 * <ul>
 * <li>{@link Message}</li>
 * </ul>
 * Produces:
 * <ul>
 * <li>{@link Message}</li>
 * </ul>
 */
public abstract class Service implements ServiceIF {

	protected EventHub hub;

	public Service(EventHub hub) {
		this.hub = hub;
		hub.register(this);
	}
	
	/**
	 * handles a {@link MessageIF} notification 
	 */
	public void notify(MessageIF msg) {
		// should be overidden by services wanting to handle incoming messages
	}

	public void shutdown() {
		// default does nothing except log shutdown
		Logger.info(this, "shutting down "+ getName() +"...");
	}
	
	/**
	 * deactivates the service while system is still up.  (shutdown is 
	 * called when system is going down)
	 */
	public void deactivate() {
		hub.deregister(this);
	}
	
	public String getName() {
		return this.getClass().getSimpleName();
	}

}
