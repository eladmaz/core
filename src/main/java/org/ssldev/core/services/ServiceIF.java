package org.ssldev.core.services;

import org.ssldev.core.messages.MessageIF;

/**
 * a service wraps business logic, ideally single-responsibility-scoped,
 * that reacts to {@link MessageIF} produced and consumed by other services.
 */
@FunctionalInterface
public interface ServiceIF {
	
	public void notify(MessageIF msg);
	
	default public void init() {
		// no init logic by default
	}
	
	default public void start() {
		// no init logic by default
	}
	
	default public void shutdown() {
		// no shutdown logic by default
	}

	default public String getName() {
		return getClass().getSimpleName();
	}
	
}
