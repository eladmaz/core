package org.ssldev.core.services;

import java.util.Arrays;
import java.util.List;

import org.ssldev.core.messages.MessageIF;
import org.ssldev.core.mgmt.EventHub;
import org.ssldev.core.utils.Logger;

public class ShutdownService extends Service{
	
	private List<Class<? extends MessageIF>> messagesThatShouldTriggerShutdown;

	@SafeVarargs
	public ShutdownService(EventHub hub, Class<? extends MessageIF>... messagesThatShouldTriggerShutdown) {
		super(hub);
		this.messagesThatShouldTriggerShutdown = Arrays.asList(messagesThatShouldTriggerShutdown);
	}
	
	@Override
	public void init() {
		messagesThatShouldTriggerShutdown.forEach(clz -> {
			hub.register(clz, this::doShutdown);
		});
	}
	
	
	private void doShutdown(MessageIF msg) {
		Logger.info(this, "shutting down after receiving: "+msg);
		hub.shutdown();
	}

}
