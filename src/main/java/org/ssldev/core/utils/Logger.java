package org.ssldev.core.utils;

import static org.ssldev.core.utils.Logger.Level.DEBUG;
import static org.ssldev.core.utils.Logger.Level.ERROR;
import static org.ssldev.core.utils.Logger.Level.FINEST;
import static org.ssldev.core.utils.Logger.Level.INFO;
import static org.ssldev.core.utils.Logger.Level.TRACE;
import static org.ssldev.core.utils.Logger.Level.WARN;
import static org.ssldev.core.utils.StringUtils.stackTraceToString;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;

public class Logger {
	private static String LOG_LOC = null;
	private static boolean doClassName = true;
	private static FileWriter fw;
	private static boolean isInfoEnabled = true;
	private static boolean isDebugEnabled = false;
	private static boolean isFinestEnabled = false;
	private static boolean isTraceEnabled = false;
	private static boolean isAddTimeToOutput = false;
	
	private static Date date = new Date(System.currentTimeMillis());
	private static Format f = new SimpleDateFormat("HH:mm:ss");
	private static StringBuilder timeString = new StringBuilder();
	
	private static HashSet<String> noLogByClassName = new HashSet<>();
	
	private static int listenerId;
	private static HashMap<Integer, LoggerListener> logListeners = new HashMap<>();
	
	public enum Level {
		ERROR("[ERROR]"), WARN("[WARN]"), INFO("[INFO]"), DEBUG("[DEBUG]"), TRACE("[TRACE]"), FINEST("[FINEST]");
		
		private String loggedAs;
		private Level(String val) {
			loggedAs = val;
		}
		private String getLoggedAsValue() { return loggedAs; }
	}
	
	/**
	 * initialize the logger.  Logger will output log to file, if valid
	 * file path is given
	 * 
	 * @param filePath of log file location
	 */
	public static void init(String filePath) {
		if(null == filePath) {
			return;
		}
		
		LOG_LOC = filePath;
		try {
			Path path = Paths.get(filePath);
			
			Files.createDirectories(path.getParent());
			
			fw = new FileWriter(LOG_LOC); 
		} catch (IOException e) {
			System.err.println("Could not create Logger at " + LOG_LOC);
			e.printStackTrace();
		}
	}
	
	private Logger() {
		// null
	}

	private static String getTime() {
		timeString.setLength(0); // reset
		date.setTime(System.currentTimeMillis());
		timeString.append("[").append(f.format(date)).append("]");
		return timeString.toString();
	}
	
	public static void println() {
		p("");
	}
	
	public static void error(Object obj, String msg) {
		print(ERROR, obj.getClass(), msg);		
	}
	public static void error(Object obj, String msg, Throwable t) {
		print(ERROR, obj.getClass(), msg, t);		
	}

	public static void debug(Object obj, String msg) {
		if(isDebugEnabled && !isExcluded(obj)) {
			print(DEBUG, obj.getClass(), msg);
		}
	}
	public static void trace(Object obj, String msg) {
		if(isTraceEnabled && !isExcluded(obj)) {
			print(TRACE, obj.getClass(), msg);
		}
	}

	public static void finest(Object obj, String msg) {
		if(isFinestEnabled && !isExcluded(obj))
			print(FINEST, obj.getClass(), msg);
	}
	
	public static void info(Object obj, String msg) {
		if(isInfoEnabled && !isExcluded(obj))
			print(INFO, obj.getClass(), msg);
	}
	public static void warn(Object obj, String msg) {
		print(WARN,obj.getClass(), msg);
	}

	
	
	private static void print(Level level, Class<?> clz, String msg) {
		
		logListeners.values().forEach(l -> l.logged(level, clz, msg));
		
		StringBuilder sb = new StringBuilder();
		if(isAddTimeToOutput)sb.append(getTime());
		sb.append(level.loggedAs).append(" ");
		sb.append(doClassName ? clz.getSimpleName() + ": " : "");
		sb.append(msg);
		
		p(sb.toString());
	}
	
	private static void print(Level level, Class<?> clz, String msg, Throwable t) {
		print(level, clz, msg + "\n" + stackTraceToString(t));
	}
	

	private static void p(String o) {
		// print to console
		System.out.println(o);
		
		// print to file, if enabled
		if(null != fw) {
			try {
				fw.write(o);
				fw.write("\n");
				fw.flush();
			} catch (IOException e) {
				p(ERROR.getLoggedAsValue() + " failed to write to log file due to "+e);
				e.printStackTrace();
				close();
				fw = null;
			}
		}
	}

	public static void close() {
		info(Logger.class, "closing the log..");
		if(null !=fw) {
			try {
				fw.close();
			} catch (IOException e) {
				p(ERROR.getLoggedAsValue() + "cannot close filewriter due to "+e);
				e.printStackTrace();
				fw = null;
			}
		}
	}

	public static void enableDebug(boolean b) {
		isDebugEnabled |= b;
	}
	public static void enableFinest(boolean b) {
		isFinestEnabled |= b;
	}
	public static void enableTrace(boolean b) {
		isTraceEnabled |= b;
	}
	
	public static void setShowTime(boolean b) {
		isAddTimeToOutput |= b;
	}

	/**
	 * @return true if Log level is debug or finest
	 */
	public static boolean isDebugEnabled() {
		return isDebugEnabled || isFinestEnabled;
	}
	
	public static boolean isTraceEnabled() {
		return isTraceEnabled;
	}

	public static void excludeClass(String name) {
		noLogByClassName.add(name);
	}
	
	private static boolean isExcluded(Object obj) {
		return noLogByClassName.contains(obj.getClass().getName());
	}

	
	/**
	 * adds a {@link LoggerListener} to be notified of log events
	 * @param listener of log events
	 * @return handle to call when wanting to deregister from events
	 */
	public static Runnable addLoggerListener(LoggerListener listener) {
		final int id = listenerId++;
		logListeners.put(id, listener);
		
		return () -> logListeners.remove(id);
	}
	/**
	 * a listener of logging events
	 */
	public interface LoggerListener {
		
		/**
		 * notification of a log write event
		 * @param level of log event
		 * @param clz logging message
		 * @param msg message to be logged
		 */
		public void logged(Level level, Class<?> clz, String msg);
		
	}

}
