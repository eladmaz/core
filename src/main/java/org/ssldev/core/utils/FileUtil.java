package org.ssldev.core.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Optional;

public class FileUtil {
	
	/**
	 * get file created-at attribute 
	 * @param f to check
	 * @return {@link FileTime} of creation or empty optional
	 */
	public static Optional<FileTime> creationTime(File f) {
		try {
		    BasicFileAttributes attr = Files.readAttributes(f.toPath(), BasicFileAttributes.class);
		    return Optional.of(attr.creationTime());
		} catch (IOException ex) {
		    // handle exception
			Logger.error(FileUtil.class, "encountered error trying to set date added for: "+f,ex);
		}
		
		return Optional.empty();
	}
	
	/**
	 * @return current time as {@link FileTime}
	 */
	public static FileTime now() {
		return FileTime.from(Instant.now());
	}
	
	private static ZonedDateTime toZonedDateTime(FileTime fileTime) {
		return fileTime.toInstant().atZone(ZoneId.systemDefault());
	}
	
	public static LocalDate toLocalDate(FileTime fileTime) {
		return LocalDate.from(toZonedDateTime(fileTime));
	}
	
	public static Date toDate(FileTime fileTime) {
		return Date.from(toZonedDateTime(fileTime).toInstant());
	}

}
