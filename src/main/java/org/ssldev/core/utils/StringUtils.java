package org.ssldev.core.utils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

public class StringUtils {
	private static DateTimeFormatter formatter =
		    				DateTimeFormatter.ofLocalizedDateTime( FormatStyle.SHORT )
		                     .withLocale( Locale.getDefault() )
		                     .withZone( ZoneId.systemDefault() );

	/**
	 * Stringifies the given exception
	 * 
	 * @param t exception to stringify
	 * @return exception stacktrace in string form
	 */
	public static String stackTraceToString(Throwable t) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		t.printStackTrace(pw);
		return sw.toString();
	}
	
	/**
	 * stringifies given time into string format (ex: 02/06/15 14:34)
	 * @param milisSinceEpoch to stringify
	 * @return time in string format
	 */
	public static String toDateAndTime(long milisSinceEpoch) {
		return formatter.format(Instant.ofEpochMilli(milisSinceEpoch));
	}
	
}
