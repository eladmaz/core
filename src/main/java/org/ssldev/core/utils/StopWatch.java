package org.ssldev.core.utils;

import java.time.Duration;
import java.time.Instant;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * used to measure execution time of things.
 * <p>
 * calling 'toString()' on a watch will print out the time-elapsed (if started)
 * or time between start and stop (if stopped).
 * <p>
 * Some output examples: <br>
 * <ul>
 * <li> 1 millis     = 0.001s </li>
 * <li> 1 seconds    = 1.00s </li>
 * <li> 1000 seconds = 16m 40.000s </li>
 * </ul> 
 */
public class StopWatch {
	
	private Instant startTime;
	private Instant stopTime;
	
	/**
	 * starts the watch
	 * @return reference to this watch
	 */
	public StopWatch start() {
		startTime = Instant.now();
		return this;
	}
	
	/**
	 * stops the watch
	 * @return reference to this watch
	 */
	public StopWatch stop() {
		stopTime = Instant.now();
		return this;
	}

	/**
	 * get the time elapsed since last time stop was called, or {@link Instant#now()}
	 * if watch has not been stopped yet.
	 * @param unit of time granularity
	 * @return time in given units.  or zero if granularity is too large.
	 */
	public long elapsed(TimeUnit unit) {
		Instant nonNullEnd = null == stopTime? Instant.now() : stopTime;
		Duration elapsedDuration = Duration.between(startTime, nonNullEnd).abs();
		
		return unit.convert((elapsedDuration.toMillis()), TimeUnit.MILLISECONDS);
	}
	
	
	private static String durationBetween(Instant start, Instant end) {
		Objects.requireNonNull(start);
		Instant nonNullEnd = null == end? Instant.now() : end;
		
		return Duration.between(start, nonNullEnd).abs().toString()
				.substring(2)
	            .replaceAll("(\\d[HMS])(?!$)", "$1 ")
	            .toLowerCase();
	}
	
	@Override
	public String toString() {
		if(null == stopTime && null == startTime) {
			return "not started";
		}
		else {
			return durationBetween(startTime, stopTime);
		}
	}
	
}
