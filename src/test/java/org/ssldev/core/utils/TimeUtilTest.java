package org.ssldev.core.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import org.junit.Test;
import org.ssldev.core.utils.TimeUtil.MONTH;

public class TimeUtilTest {

	@Test
	public void toLocalDateFromCurrentTimeOfEpoch() {
		assertEquals(LocalDate.now(),TimeUtil.toLocalDate(System.currentTimeMillis()));
	}
	
	@Test public void localDateParsing() {
		
		assertEquals(LocalDate.parse("2018-07-01"), TimeUtil.toLocalDate(MONTH.JUL, 2018));
	}
	@Test public void dateIsBetween () {
		LocalDate a = TimeUtil.toLocalDate(MONTH.JUL, 2018);
		LocalDate b = TimeUtil.toLocalDate(MONTH.JUL, 2017);
		LocalDate c = TimeUtil.toLocalDate(MONTH.AUG, 2018);
		
		assertTrue(TimeUtil.isBetween(a, b, c));
	}
	

}
